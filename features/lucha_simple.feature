# language: es

Característica: Lucha simple

Ejemplo: Humano contra humano es empate
  Dado un humano
  Cuando lucha contra un humano
  Entonces el resultado es empate

Ejemplo: Vampiro contra humano gana
  Dado un vampiro
  Cuando lucha contra un humano
  Entonces el resultado es gana vampiro

Ejemplo: Lobo contra humano gana
  Dado un lobo
  Cuando lucha contra un humano
  Entonces el resultado es gana lobo
